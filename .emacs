;; Set up package archives
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;; Ensure 'use-package' is installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; Basic UI customizations
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)

;; Evil mode for Vim keybindings
(use-package evil
  :config
  (evil-mode 1))

;; Theme
(use-package modus-themes
  :config
  (load-theme 'modus-operandi t))

;; JavaScript and TypeScript setup
(use-package web-mode
  :mode (("\\.jsx?\\'" . web-mode)
         ("\\.tsx\\'" . web-mode))
  :config
  (setq web-mode-content-types-alist
        '(("jsx" . "\\.js[x]?\\'")
          ("tsx" . "\\.tsx\\'")))
  (setq web-mode-markup-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-script-padding 2
        web-mode-style-padding 2))

;; LSP for JavaScript and TypeScript
(use-package lsp-mode
  :hook ((web-mode . lsp-deferred))
  :commands lsp
  :config
  (setq lsp-prefer-flymake nil))

;; Company mode for autocompletion
(use-package company
  :hook (web-mode . company-mode)
  :config
  (setq company-tooltip-align-annotations t))

;; Prettier for code formatting
(use-package prettier-js
  :hook (web-mode . prettier-js-mode))

;; Treemacs for project navigation
(use-package treemacs
  :config
  (setq treemacs-width 30
        treemacs-height 40
        treemacs-theme "Material"
        treemacs-icon-theme "all-the-icons"
        treemacs-git-mode 'simple
        treemacs-project-follow-mode t)
  (add-hook 'treemacs-mode-hook (lambda () (treemacs-resize-icons 16)))
  (global-set-key (kbd "C-c t") 'treemacs))

;; Debugging with dap-mode
(use-package dap-mode
  :config
  (require 'dap-chrome))

;; Next.js specific configuration
(use-package js2-mode
  :mode "\\.js\\'"
  :config
  (setq js-indent-level 2))

(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . rjsx-mode))

;; Optional: Helm for improved navigation
(use-package helm
  :config (helm-mode))

;; Optional: Projectile for project management
(use-package projectile
  :config (projectile-mode))

;; Optional: Helm Projectile for Helm integration with Projectile
(use-package helm-projectile
  :config (helm-projectile-on))

;; Org mode styling for header bullets
(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

;; Optional: Neotree for file tree navigation
(use-package neotree
  :config
  (global-set-key [f8] 'neotree-toggle))

;; TypeScript and JSX setup with web-mode
(use-package web-mode
  :mode (("\\.tsx\\'" . web-mode))
  :config
  ;; Adjust indentation
  (setq web-mode-markup-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-script-padding 2
        web-mode-style-padding 2)
  ;; Associate js2-mode with JavaScript blocks in web-mode
  (setq web-mode-content-types-alist '(("jsx" . "\\.tsx\\'")))
  (add-to-list 'web-mode-engines-alist '("jsx" . "js2-mode"))
  ;; Enable auto-closing and auto-pairing
  (setq web-mode-enable-auto-closing t
        web-mode-enable-auto-pairing t)
  ;; Enable CSS colorization
  (setq web-mode-enable-css-colorization t))

;; Set up js2-mode for JavaScript syntax highlighting
(use-package js2-mode
  :mode "\\.js\\'"
  :config
  (setq js-indent-level 2))

;; Associate web-mode with HTML files
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(use-package dap-mode
  :after lsp-mode
  :config
  (require 'dap-chrome)
  (dap-register-debug-template
   "Next.js"
   (list :type "chrome"
         :request "launch"
         :name "Launch Next.js"
         :url "http://localhost:3000"
         :webRoot "${workspaceFolder}"
	 :runtimeExecutable "~/usr/bin/chromium"
         :sourceMaps t)))

;; Set Startup theme
(load-theme 'modus-vivendi t)

;; Run treemacs on startup
(add-hook 'emacs-startup-hook 'treemacs)

;;Org mode normalize Tab usage after adding Evil mode
(with-eval-after-load 'evil-maps (define-key evil-motion-state-map (kbd "TAB") nil))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("0f76f9e0af168197f4798aba5c5ef18e07c926f4e7676b95f2a13771355ce850" default))
 '(package-selected-packages
   '(org-bullets neotree helm-projectile projectile helm js2-mode dap-mode treemacs prettier-js company lsp-mode web-mode modus-themes evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
